from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^index/$', 'CoolReader.views.index'),
    url(r'^index/(?P<path>.*)$', 'django.views.static.serve', {
            'document_root': '/home/cwj/Workplaces/CoolReader/static',
        }), 
    # url(r'^CoolReader/', include('CoolReader.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)
